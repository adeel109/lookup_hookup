import csv, json
import sys

#def create_map(gform_csv, json_file):
def create_map(gform_csv):
    reader_gform = csv.DictReader(open(gform_csv))
    dict_score_attend = {}
    for row in reader_gform:
        inputadm_integer = row['inputadm']
        if len(inputadm_integer) == 4 and inputadm_integer.isdigit():
            if int(row['adm']) == int(row['inputadm']):
                key = row.pop('adm')
                if key in dict_score_attend:
                    print("{0} admission number is repeated!!".format(dict_score_attend[key]))
                    pass
                dict_score_attend[key] = row
                dict_score_attend[key]['attendance'] = 'P'
            else:
                print("Email adm no. --> {0}@saintjohnsacademy.com".format(row['adm']))
                print("Input adm no. --> {0}".format(row['inputadm']))
                print("Manual review advised")
                key = row.pop('inputadm')
                if key in dict_score_attend:
                    print("{0} admission number is repeated!!".format(dict_score_attend[key]))
                    pass
                dict_score_attend[key] = row
                dict_score_attend[key]['attendance'] = 'P'
        else:
            print("Admission number input for {0}@saintjohnsacademy.com is wrong. Please fix it".format(row['adm']))
            sys.exit()
    # with open(json_file, 'w') as jsonFile:
    #     jsonFile.write(json.dumps(dict_score_attend, indent=4))
    return dict_score_attend

def create_class_list(csv_class):
    reader_class = csv.DictReader(open(csv_class))
    class_list_of_dict = []
    for row in reader_class:
        class_list_of_dict.append(row)
    return class_list_of_dict

def get_bma(score_map, class_info):
    for item in class_info:
        if item['adm'] in score_map.keys():
            item['marks'] = score_map[item['adm']]['marks']
            item['attendance'] = score_map[item['adm']]['attendance']
        else:
            item['marks'] = ''
            item['attendance'] = ''
    return class_info

def get_attendance(score_map, class_info):
    for item in class_info:
        if item['adm'] in score_map.keys():
            item['attendance'] = score_map[item['adm']]['attendance']
        else:
            item['attendance'] = ''
    return class_info

def get_marks(score_map, class_info):
    for item in class_info:
        if item['adm'] in score_map.keys():
            item['marks'] = score_map[item['adm']]['marks']
        else:
            item['marks'] = ''
    return class_info

def print_csv(csv_rows, csv_columns, class_sec):
    csv_output = class_sec
    try:
        with open(csv_output, 'w') as csvOutput:
            writer = csv.DictWriter(csvOutput, fieldnames=csv_columns)
            writer.writeheader()
            for data in csv_rows:
                writer.writerow(data)
    except IOError:
        print("I/0 Error")

if __name__ == '__main__':
    map_file = sys.argv[1]
    class_sheet = sys.argv[2]
    task = sys.argv[3]
    # print_to = 'thisisjson.json'
    # gform_map = create_map(map_file, print_to)
    gform_map = create_map(map_file)
    class_list = create_class_list(class_sheet)
    
    if task == "bma":
        updated_class_info = get_bma(gform_map, class_list)
        csv_columns = ['roll','adm','name','marks','attendance']
        print_csv(updated_class_info, csv_columns, class_sheet)
    elif task == "attendance":
        updated_class_info = get_attendance(gform_map, class_list)
        csv_columns = ['roll','adm','name','attendance']
        print_csv(updated_class_info, csv_columns, class_sheet)
    else:
        updated_class_info = get_marks(gform_map, class_list)
        csv_columns = ['roll','adm','name','marks']
        print_csv(updated_class_info, csv_columns, class_sheet)
