print_usage() {
    echo "Usage : ./get_csv.sh -b if both, marks and attendance, needed in resultant csv"
    echo "                     -a if attendance needed in resultant csv"
    echo "                     -m if marks needed in resultant csv"
    echo "                     --gform <google form csv, with no field names, inside csveez dir> (top order - timestamp, email, marks)"
    echo "                     --stnd <directory with all section CSV's in it> (create directory to put all section csv files eg., 6 or 6th or sixth"
    echo "                     and a.csv, b.csv, etc for csv files inside the directory, with no field names)"
}

export PATH="/usr/local/opt/gnu-getopt/bin:$PATH"
marks='no'
attendance='no'
bma='no' #both marks and attem onlyadm onlyscore rawadmscore trimadmscor    ndance
gform=''
csheet=''
stnd=''

TEMP=`getopt -o bam --long gform:,csheet:,stnd: -- "$@"`

eval set -- "$TEMP"
# extract options and their arguments into variables.
while true ; do
    case "$1" in
        -b)
            bma="yes"
            echo "Get both marks and attendance"
            shift 1
            ;;
        -a)
            attendance="yes"
            echo "Get attendance only"
            shift 1
            ;;
        -m)
            marks="yes"
            echo "Get marks only"
            shift 1
            ;;
        --gform)
            gform=$2
            shift 2
            ;;
        --stnd)
            stnd=$2
            shift 2
            ;;
        --) shift
            break
            ;;
        *) echo "Unsupported Param "$1
        exit 1
        ;;
    esac
done

if [ "$gform" == "" -o "$stnd" == "" ]
then
    print_usage 1
    exit 1
fi

if [ "$marks" == "no" -a "$attendance" == "no" -a "$bma" == "no" ]
then
    print_usage 2
    exit 1
fi

echo "gform -- $gform"

#some bash operations on gform csv
cut -d, -f2 < $gform > rawadm
cut -d@ -f1 < rawadm > onlyadm
cut -d, -f6 < $gform > onlyinputadm
cut -d, -f3 < $gform > rawscore
while read -r line; do echo $line | cut -d/ -f1 | awk '{$1=$1};1' ; done < rawscore > onlyscore
paste onlyadm onlyinputadm onlyscore > trimadmscore
tr -s '[:blank:]' ',' < trimadmscore > csveez/admscore.csv
gform='csveez/admscore.csv'
sed -i '1 i\adm,inputadm,marks' $gform
#cat $gform
rm onlyadm onlyinputadm onlyscore rawscore rawadm trimadmscore
#exit 1
#looping through class csv files
echo "stnd -- $stnd"
for csheet in $stnd/*.csv; do
    #some bash operations on class csv
    cp $csheet "${csheet//\//_}"
    csheet="${csheet//\//_}"
    sed -i '1 i\roll,adm,name' $csheet
    if [ $bma == "yes" -a $attendance = "no" -a $marks == "no" ]
    then
        python3 csv_lookup_and_hookup.py $gform $csheet "bma"
    fi

    if [ $attendance == "yes" -a $bma == "no" -a $marks == "no" ]
    then
        python3 csv_lookup_and_hookup.py $gform $csheet "attendance"
    fi

    if [ $marks == "yes" -a $attendance = "no" -a $bma == "no" ]
    then
        python3 csv_lookup_and_hookup.py $gform $csheet "marks"
    fi
done
echo "Please check class csv files in the same location"
rm $gform
